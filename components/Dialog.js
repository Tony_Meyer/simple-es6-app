const popupS = require('popups');
const Dialog = (data) => {
	popupS.modal({
	    title:   data.name,
			content: {
			        tag: 'div#modal',
							children:[
			            {
										tag: 'img#modalImage',
						        src: data.brandThumbnailImage,
		                css: {
		                    width: '200px'
		                }
			            },
			            {
										tag: 'p',
										text: data.shortDescription,
		                css: {
		                    width: '100%'
		                }
			            }
			        ]
			    }
	});
};

export default Dialog;
