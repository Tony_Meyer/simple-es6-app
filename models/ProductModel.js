export default {
	sku: {
		type: 'number'
	},
	name: {
		type: 'string'
	},
	regularPrice: {
		type: 'number'
	},
	salePrice: {
		type: 'number'
	},
	shortDescription: {
		type: 'string'
	}
};
