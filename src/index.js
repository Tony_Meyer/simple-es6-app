'use strict';
require('./main.css');

import {ALL_PRODUCTS, CATEGORIES} from './../api/EndPoints';
import CategoryModel from './../models/CategoryModel';
import Dialog from './../components/Dialog';
import FETCH from './../api/FETCH';
import ProductBox from './../components/ProductBox';
import REQUESTCATEGORY from './../api/REQUESTCATEGORY';
import REQUESTDETAILS from './../api/REQUESTDETAILS';
import SideBarItem from './../components/SideBarItem';

class ES6App {
  // Initializes the app.
  constructor() {
    // Shortcuts to DOM Elements.
		this.categoriesButtonParent = document.getElementById('categories');
		this.productContainer = document.getElementById('product-container');
		this.grid = document.getElementById('grid');
		this.categories = document.getElementById('categories');

    // Listen to category button in sidebar.
    this.categoriesButtonParent.addEventListener('click', () => this.filterProducts(event));
    this.grid.addEventListener('click', () => this._getDetails(event));

		// Render categories in sidebar
		this._renderCategories();

		// Render all products by default
		this._renderAllProducts();
  }

	/**
	 * This function makes an API request with a category id then renders the currently selected category's products
	 *
	 * @method _renderCategoryProducts
	 * @param      {number}    id    		- Category id
	 */
	_renderCategoryProducts(id)  {
		REQUESTCATEGORY(id).then((response) => {

			// remove old products if there are any on the page
			if (document.querySelector('.product-box') !== null) {
			    this.removeElementsByClass('product-box');
			}
			const objectArr = response.products;
			objectArr.forEach((item) => {
				ProductBox(this.grid, item);
			});
		});
	}

	/**
	 * This function renders the categories in the sidebar
	 *
	 * @method _renderCategories
	 */
	_renderCategories()  {
		FETCH(CATEGORIES).then((response) => {
			const objectArr = response.subCategories
			objectArr.forEach((item) => {
				SideBarItem(this.categories, item);
			});
		});
	}

	/**
	 * This function renders all avalilable product from the API
	 *
	 * @method _renderAllProducts
	 */
	_renderAllProducts() {
		FETCH(ALL_PRODUCTS).then((response) => {
			const objectArr = response.products;
			objectArr.forEach((item) => {
				ProductBox(this.grid, item);
			});
		});
	}

	/**
	 * This function renders product details in a modal window
	 *
	 * @method _renderProductDetails
	 */
	_renderProductDetails(sku) {
		REQUESTDETAILS(sku).then((response) => {
			Dialog(response);
		});
	}

	/**
	 * Utility function to remove old products before rendering new ones
	 *
	 * @method removeElementsByClass
	 */
	removeElementsByClass (className) {
	    var elements = document.getElementsByClassName(className);
	    while(elements.length > 0){
	        elements[0].parentNode.removeChild(elements[0]);
	    }
	}

	/**
	 * This function grabs the category id from sidebar button (category product ids are rendered as the elements id)
	 * and passes it to the API request function
	 *
	 * @method filterProducts
	 * @param      {object}    event    		- Click event
	 */
	filterProducts(event) {
		if(!event) return false;
		event.preventDefault();
		const categoryId = event.target.id;
		this._renderCategoryProducts(categoryId);
	}

	/**
	 * This function grabs the category id from sidebar button (category product ids are rendered as the elements id)
	 * and passes it to the API request function
	 *
	 * @method filterProducts
	 * @param      {object}    event    		- Click event
	 */
	_getDetails(event) {
		if(!event) return false;
		event.preventDefault();
		const sku = event.target.id;
		this._renderProductDetails(sku);
	}

}

// On load start the app.
window.addEventListener('load', () => new ES6App());
