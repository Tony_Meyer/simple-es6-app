import ProductModel from './../models/ProductModel';

const ProductBox = (refNode, data) => {

	const productTemplate = [
		`<div class="demo-card-wide mdl-card mdl-shadow--2dp" id="${data.sku}">`,
		  `<div class="mdl-card__title" id="${data.sku}" style="background: url('${data.thumbnailImage}') center / cover;"></div>`,
		  `<div class="mdl-card__supporting-text">${data.name}</div>`,
		  `<div class="mdl-card__supporting-text">Regular Price: $${data.regularPrice}</div>`,
		`</div>`
	].join('');

	const item = document.createElement('div');
	item.innerHTML = productTemplate;
	const classes = ['product-box', 'mdl-cell', 'mdl-cell--3-col'];
	item.classList.add(...classes);
	refNode.insertBefore(item, null);
};

export default ProductBox;
