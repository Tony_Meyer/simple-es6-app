const $ = require('jquery');
const FETCH = (ENDPOINT) => {
		return(
			$.ajax({
			    url: ENDPOINT,

			    // jsonp Was neccessary to successfully call the api from the app
			    dataType: "jsonp",

			    data: {format: "json"},

			    success: ( response ) => {}
			})
		);
};
export default FETCH;
