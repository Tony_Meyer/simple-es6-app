// To run the tests in the terminal, uncomment chai require statement and run: 'npm run test'
var chai = require('chai');
var AddClass = require('./../src/utils/AddClass');
// This var declaration is for use in the testrunner.html file
var assert = chai.assert;

describe('Array', function() {
  it('should start empty', function() {
    var arr = [];

    assert.equal(arr.length, 0);
  });
});

describe('addClass', function() {

  it('should add class to element', function() {
    var element = { className: '' };

    AddClass(element, 'test-class');

    assert.equal(element.className, 'test-class');
  });

	it('should not add a class which already exists', function() {
	  var element = { className: 'exists' };

	  AddClass(element, 'exists');

	  var numClasses = element.className.split(' ').length;
	  assert.equal(numClasses, 1);
	});
});
