const ALL_PRODUCTS = 'http://www.bestbuy.ca/api/v2/json/search?categoryid=departments';
const CATEGORIES = 'http://www.bestbuy.ca/api/v2/json/category/Departments';

export {
	ALL_PRODUCTS,
	CATEGORIES
}
