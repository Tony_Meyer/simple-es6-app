const _get = (item) => JSON.parse(localStorage.getItem(item));
const _put = (item) => JSON.stringify(localStorage.setItem(item));

export {_get, _put};
