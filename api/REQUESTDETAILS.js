const $ = require('jquery');
const REQUESTDETAILS = (sku) => {
		return(
			$.ajax({
			    url: `http://www.bestbuy.ca/api/v2/json/product/${sku}`,

			    // jsonp Was neccessary to successfully call the api from the app
			    dataType: "jsonp",

			    data: {format: "json"},

			    success: ( response ) => {}
			})
		);
};
export default REQUESTDETAILS;
