import CategoryModel from './../models/CategoryModel';

const SideBarItem = (refNode, data) => {
		const item = document.createElement('a');
		const classes = ['category-item', 'mdl-navigation__link'];
		item.id = data.id;
		item.innerText = data.name;
		item.setAttribute('href','#');
		item.classList.add(...classes);
  	refNode.insertBefore(item, null);
};

export default SideBarItem;
