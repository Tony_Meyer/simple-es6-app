const $ = require('jquery');
const REQUESTCATEGORY = (id) => {
		return(
			$.ajax({
			    url: `http://www.bestbuy.ca/api/v2/json/search?categoryid=${id}`,

			    // jsonp Was neccessary to successfully call the api from the app
			    dataType: "jsonp",

			    data: {format: "json"},

			    success: ( response ) => {}
			})
		);
};
export default REQUESTCATEGORY;
